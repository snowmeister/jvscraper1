// Axios to GET the HTML from the remote site...
const axios = require('axios');
// cheerio - Use to manipulate the HTML returned...
const cheerio = require('cheerio');
// Allows us to save to the filesystem....
const fs = require('fs');
// The URL we will be scraping...
const url = 'http://toprank.domains'
// This is the name of the source site that will appear in the spreadsheet
const source = 'toprank'
// This is the name of the file we are writing to....
const filename = 'domains.csv';
// This is the TARGET ELEMENT we want the scraper to look for...
const target = 'div.productbox'
// This is the line number we want to go from so we have consistent numbering in our CSV
let j = 502;

// Do the get...
console.log('getting the URL...')
axios.get(url)
     .then((response)=> { // Then, once we have the response...
       console.log('Got a response...')
       if(response.status == 200){ // If its a 200, all is good...
         console.log('Response is good...')
         // lets grab the HTML itself...
         const html = response.data;
         // and lets use cheerio to deal with it
         const $ = cheerio.load(html);
         // create a header row for the CSV which will contain the domains we find...
         let domainsList = '';
         // Find all the A elements with a class="brand"...then loop over it...
         $(target).each((i, elem)=>{
            // grab the TITLE attribute of the current A element we have found in the loop...
            let domain = $(elem).find('a.prodhref').attr('title');
            // log it out to the console...
            console.log('Found ' + domain);
            // push it into CSV
            domainsList+= `${j+1}, ${domain.toLowerCase()}, ${source}\n`;
            j++
         })
        /// Write the CSV to the filesystem!
         fs.appendFile(filename, domainsList, (err)=>{
           // If something went wrong writing the file...
           if(err) { console.log(err)}
           else{
             // Otherwise, confirm the file has been created!
             console.log('Created ' + filename + ' successfully');
           }
         })
       } else{ // If its NOT a 200 response, something is amiss!
         console.log('Something went wrong getting the URL!')
       }
     })
