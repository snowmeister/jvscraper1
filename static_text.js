// Axios to GET the HTML from the remote site...
const axios = require('axios');
// cheerio - Use to manipulate the HTML returned...
const cheerio = require('cheerio');
// Allows us to save to the filesystem....
const fs = require('fs');
const _ = require('lodash');
// We  need to be able to read a file line by line....
const LineByLineReader = require('line-by-line');
const filename = 'domains.csv';
const gofromline = 554
const lr = new LineByLineReader('sedo-1.txt');
const source = 'Dauman';
let line = 0
let domains = [];

const cleanList = (array)=>{
  console.log('Cleaning....')
  console.log(array.length);
  let cleaned = [];
  array.map(item=>{
    if(typeof(item) == 'undefined'){
      console.log('ignore me...');
    }else{
      cleaned.push(item);
    }
  });
  return cleaned;
}

const writeToCSV = (array) =>{
  let domainsList = '';
  let line = gofromline
  array.map((item)=> {
    domainsList+= `${line}, ${item}, ${source}\n`;
    line++;
  })
  fs.appendFile(filename, domainsList, (err)=>{
    // If something went wrong writing the file...
    if(err) { console.log(err)}
    else{
      // Otherwise, confirm the file has been created!
      console.log('Created ' + filename + ' successfully');
    }
  })
}

lr.on('error', function (err) {
  console.log('OOHHNOOOOOOOOOOO');
});

lr.on('line', function (line) {
  const $ = cheerio.load(line);
  const target = $('input[type="checkbox"]').each((i, elem)=>{
      const domain = $(elem).attr('data-domain');
      //cconsole.log('working');
      domains.push(domain)
  });
});

lr.on('end', function () {
  writeToCSV(cleanList(domains));
  console.log('Done')
});
